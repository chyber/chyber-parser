var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var personSchema = new Schema({ name : {
				first: { type: String, required: true, trim: true, },
			        last: { type: String, required: true, trim: true, },
			      },
			      age: { type: Number, min:0},
			      username: {type: String, unique: true, trim: true},
			      password: { type: String, trim: true},
				  groups: [{type: Schema.ObjectId, ref: 'Group'},],
			}); //need to store password encrypted

var Person = mongoose.model('Person', personSchema); 
module.exports=Person;

