var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var userSchema = new Schema({ name : {
				first: { type: String, required: true, trim: true, },
			        last: { type: String, required: true, trim: true, },
			      },
			      username: {type: String, trim: true},
			      password: {type: String, trim: true},
			      age: { type: Number, min:0},
			}); //need to store password encrypted

var User = mongoose.model('User', userSchema); 
module.exports=User;

