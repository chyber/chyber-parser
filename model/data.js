//export a module using mongoose for users
//TODO: need to test validity - use strict???
//
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var dataSchema = new Schema({ 
			      person : {type: Schema.ObjectId, ref: 'Person'},
			      starttime: { type: Date, required: true, },
			      samplerate: { type: Number, required: true,default:-1},
 			      points: [Number]	});

var Data = mongoose.model('Data', dataSchema);

module.exports=Data;


