var mongoose = require('mongoose');
var User = require('./model/user');
var mongourl = require('./config/database')

var url = mongourl.localUrl;
var db = mongoose.connect(url);
console.log('database connected')

var newUser = new User({name:{first:'robert', last: 'sontag'});

//save should store them only in upper case ???
newUser.save();
console.log('user created')
db.disconnect();
