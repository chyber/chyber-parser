//all the dependencies we need
var http = require('http'); //this is a built in package that comes with node and there is no need to install it
var express = require('express');
var mongoose = require('mongoose');
 
//new app that will work on port 3000
var app = express();
app.set('port', 3000);
 
//Simple 404 response because we don't have any routing yet
app.use(function(req,res){
    res.status(404);
    res.type('text/plain');
    res.send('404');
});
 
//start the application and let node listen for requests on port 3000
http.createServer(app).listen(app.get('port'), function(){
    console.log( 'Server started on http://localhost:' + app.get('port') + '; press Ctrl-C to terminate.' );
});
