//TODO:
//1. make this a raw upload of the csv so we have raw data, then configure a model to parse back
//   a. name object saved, and passed to file call - not sure about this
//   b. convert the timestamp the start time, how to manage end time
//   c. how to manage sampling time
//   d. understand the end time of the data
//2. need a default upload directory, how to log other files via a config???
//3. read back the data
//4. relate data to a person / entity this is done using the ref id

//logic to parse CSV or other file types

var DEFAULT_UPLOAD_DIR='./data';



var parse = function() {

	//adds a default set of entities to db

	function parsePerson(path){
		var csv = require("fast-csv");
        csv.fromPath(path, {headers : ["first", "last","username","password", "age"],ignoreEmpty: true})
        	.on("data", function(data){
	        	console.log("getting data",data, lines);
	    	    var entityDoc = new Data();
    	    	entityDoc.name.first = data.first;
        	    entityDoc.name.last = data.last;
            	entityDoc.username=data.username;
		        entityDoc.password=data.password;
    	        entityDoc.age=parseInt(data.age);
    		    entityDoc.save();
        		lines=lines+1;
	        })
    	    .on("end", function(){
        	    console.log("done");
	        });
	}

	//TODO: how to manage multiple types of schema in files
	//      run over the other csv files currently only looking at one - which one
	//      should probably autoupload an array of files and use there names ???
	//      files already contain date of upload

	function parseData(username, path){
		var csv = require("fast-csv");
		var Person = require("./model/person);
		var Data = require("./model/data");
        var lines = 0, csvDoc = new Data();

        entity=Person.FindOne({'username': username});
        csvDoc.person=entity._id;
        csv.fromPath(path, {headers : false})
            .on("data", function(data){
                lines+=1;
                console.log("getting data: %s %d",data[0], lines);
                var type = typeof(data);
                if(type=='undefined'){
                console.log('undefined value');
                }
                else if(csvDoc.starttime==-1){
				   //this is the unix timestamp so multiply by 1000
                   csvDoc.starttime=parseInt(data[0])*1000;
                }else if(csvDoc.samplerate==-1){
                   csvDoc.samplerate=parseInt(data[0]);
                }else{
                   csvDoc.points.push(parseFloat(data[0]));
                    }
            })
            .on("end", function(){
                console.log("saving");
                csvDoc.save();
                    console.log("done");
            });
	}

	//var callbacks = { parsePerson : parsePerson, parseData, parseData }; 
	
	//parse the csv data based on the entity type
	function parseCSV(type, path, username){
		var mongoose = require('mongoose');
		var fs = require('fs');
		if( typeof(path)=='undefined') path = DEFAULT_UPLOAD_DIR + 'temp2.csv';
		console.log("parsinv file",path);
		var mongourl = require('./config/database')["csvUrl"];
		var callback = callbacks[type];
		try {
		  stats = fs.statSync(path);
		  console.log("File exists:%s",path);
		}
		catch (e) {
		  console.log("File does not exist: %s", path);
		}

		var db = mongoose.connection;
		var connection = mongoose.connect(mongourl);

		db.on('error', function (err) {
			console.log('connected ' + err.stack);
		});
		db.on('disconnected', function(){
			console.log('disconnected');
		});
		db.on('connected',function(callback){
			if(type=="person") parsePerson(path);
			else if(type=="data") parseData(username,path);
			else console.log("unknown parse entity %s", type);
		});

		//make sure that we are closing the connection to mongo if something happens to node (like Ctrl + C)
		process.on('SIGINT', function() {
			mongoose.connection.close(function () {
				process.exit(0);
			});
	});

	function parseStream(){;}
	
	return { parseCSV : parseCSV, parseStream : parseStream };
}
